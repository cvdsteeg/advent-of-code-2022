ThisBuild / scalaVersion := "3.2.1"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "com.jdriven.typedriven"
ThisBuild / organizationName := "jdriven"

lazy val zioVersion = "2.0.5"

lazy val root = (project in file("."))
  .settings(
    name := "advent-of-code",
    libraryDependencies ++= Seq(
      "dev.zio"                      %% "zio"         % zioVersion,
      "dev.zio"                      %% "zio-streams" % zioVersion,
      "dev.zio"                      %% "zio-test"    % zioVersion % Test
    ),
    testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework")
  )
