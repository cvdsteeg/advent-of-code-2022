package com.jdriven.typedriven.aoc2021

import zio.test.{assertTrue, ZIOSpecDefault}

object Day1Test extends ZIOSpecDefault:
  val input = List(199, 200, 208, 210, 200, 207, 240, 269, 260, 263).map(_.toString)

  def spec = suite(getClass.getSimpleName)(
    test("works with the provided example for part 1") {
      val result = Day1.part1(input)
      assertTrue(result == 7)
    },
    test("works with the provided example for part 2") {
      val result = Day1.part2(input)
      assertTrue(result == 5)
    }
  )
