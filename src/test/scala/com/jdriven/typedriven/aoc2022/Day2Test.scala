package com.jdriven.typedriven.aoc2022

import zio.test.{assertTrue, ZIOSpecDefault}

object Day2Test extends ZIOSpecDefault:
  val input = List("A Y", "B X", "C Z")

  def spec = suite(getClass.getSimpleName)(
    test("works with the provided example for part 1") {
      val result = Day2.part1(input)
      assertTrue(result == 15)
    },
    test("works with the provided example for part 2") {
      val result = Day2.part2(input)
      assertTrue(result == 12)
    }
  )
