package com.jdriven.typedriven.aoc2022

import zio.test.{assertTrue, ZIOSpecDefault}

object Day4Test extends ZIOSpecDefault:
  val input = List(
    "2-4,6-8",
    "2-3,4-5",
    "5-7,7-9",
    "2-8,3-7",
    "6-6,4-6",
    "2-6,4-8"
  )

  def spec = suite(getClass.getSimpleName)(
    test("works with the provided example for part 1") {
      val result = Day4.part1(input)
      assertTrue(result == 2)
    },
    test("works with the provided example for part 2") {
      val result = Day4.part2(input)
      assertTrue(result == 4)
    }
  )
