package com.jdriven.typedriven.aoc2022

import com.jdriven.typedriven.aoc2022.Day7.*
import zio.test.{ZIOSpecDefault, assertTrue}

import scala.collection.immutable.SortedSet

object Day7Test extends ZIOSpecDefault:
  val input = List(
    "$ cd /",
    "$ ls",
    "dir a",
    "14848514 b.txt",
    "8504156 c.dat",
    "dir d",
    "$ cd a",
    "$ ls",
    "dir e",
    "29116 f",
    "2557 g",
    "62596 h.lst",
    "$ cd e",
    "$ ls",
    "584 i",
    "$ cd ..",
    "$ cd ..",
    "$ cd d",
    "$ ls",
    "4060174 j",
    "8033020 d.log",
    "5626152 d.ext",
    "7214296 k"
  )

  """
    |- / (dir)
    |  - a (dir)
    |    - e (dir)
    |      - i (file, size=584)
    |    - f (file, size=29116)
    |    - g (file, size=2557)
    |    - h.lst (file, size=62596)
    |  - b.txt (file, size=14848514)
    |  - c.dat (file, size=8504156)
    |  - d (dir)
    |    - j (file, size=4060174)
    |    - d.log (file, size=8033020)
    |    - d.ext (file, size=5626152)
    |    - k (file, size=7214296)
    |""".stripMargin


  //@formatter:off
  val resultDir =
    Dir("/", SortedSet(
      Dir("a", SortedSet(
        Dir("e", SortedSet(
            File("i", 584)
          )),
        File("f", 29116),
        File("g", 2557),
        File("h.lst", 62596))
      ),
      File("b.txt", 14848514),
      File("c.dat", 8504156),
      Dir("d", SortedSet(
          File("k", 7214296),
          File("d.ext", 5626152),
          File("d.log", 8033020),
          File("j", 4060174))
      )
    )
  )
//@formatter:on

  def spec = suite(getClass.getSimpleName)(
    test("works with the provided example for part 1") {
      val parsed = Day7.parse(input)._1
      val result = Day7.part1(input)

      assertTrue(parsed == resultDir) &&
      assertTrue(result == 95437)
    },
    test("works with the provided example for part 2") {
      val result = Day7.part2(input)
      assertTrue(result == 24933642)
    }
  )
