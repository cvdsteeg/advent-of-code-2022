package com.jdriven.typedriven.aoc2022

import zio.test.{assertTrue, ZIOSpecDefault}

object Day6Test extends ZIOSpecDefault:
  val input = List(
    "mjqjpqmgbljsphdztnvjfqwrcgsmlb",
    "bvwbjplbgvbhsrlpgdmjqwftvncz",
    "nppdvjthqldpwncqszvftbrmjlhg",
    "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg",
    "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"
  )

  def spec = suite(getClass.getSimpleName)(
    test("works with the provided example for part 1") {
      val result = Day6.part1(input)
      assertTrue(result == List(7, 5, 6, 10, 11))
    },
    test("works with the provided example for part 2") {
      val result = Day6.part2(input)
      assertTrue(result == List(19, 23, 23, 29, 26))
    }
  )
