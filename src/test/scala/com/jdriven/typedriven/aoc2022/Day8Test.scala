package com.jdriven.typedriven.aoc2022

import com.jdriven.typedriven.aoc2022.Day7.*
import zio.test.{assertTrue, ZIOSpecDefault}

import scala.collection.immutable.SortedSet

object Day8Test extends ZIOSpecDefault:
  val input = List(
    "30373",
    "25512",
    "65332",
    "33549",
    "35390"
  )

  def spec = suite(getClass.getSimpleName)(
    test("works with the provided example for part 1") {
      val result = Day8.part1(input)

      assertTrue(result == 21)
    },
    test("works with the provided example for part 2") {
      val result = Day8.part2(input)
      assertTrue(result == 8)
    }
  )
