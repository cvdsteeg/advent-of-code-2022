package com.jdriven.typedriven.aoc2022

import zio.test.{assertTrue, ZIOSpecDefault}

object Day5Test extends ZIOSpecDefault:
  val input = List(
    "    [D]    ",
    "[N] [C]    ",
    "[Z] [M] [P]",
    " 1   2   3 ",
    "",
    "move 1 from 2 to 1",
    "move 3 from 1 to 3",
    "move 2 from 2 to 1",
    "move 1 from 1 to 2"
  )

  def spec = suite(getClass.getSimpleName)(
    test("works with the provided example for part 1") {
      val result = Day5.part1(input)
      assertTrue(result == "CMZ")
    },
    test("works with the provided example for part 2") {
      val result = Day5.part2(input)
      assertTrue(result == "MCD")
    }
  )
