package com.jdriven.typedriven.aoc2022

import zio.test.{assertTrue, ZIOSpecDefault}

object Day1Test extends ZIOSpecDefault:
  val input = List(
    "1000",
    "2000",
    "3000",
    "",
    "4000",
    "",
    "5000",
    "6000",
    "",
    "7000",
    "8000",
    "9000",
    "",
    "10000"
  )

  def spec = suite(getClass.getSimpleName)(
    test("works with the provided example for part 1") {
      val result = Day1.part1(input)
      assertTrue(result == 24000)
    },
    test("works with the provided example for part 2") {
      val result = Day1.part2(input)
      assertTrue(result == 45000)
    }
  )
