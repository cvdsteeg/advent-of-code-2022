package com.jdriven.typedriven.aoc2022

import scala.annotation.tailrec
import scala.io.Source

object Day1:
  private lazy val part1Input = Source
    .fromResource("2022/day1-1.txt")
    .getLines()
    .toList

  def part1(input: List[String] = part1Input): Int = calculate(input).max
  def part2(input: List[String] = part1Input): Int = calculate(input).sorted(Ordering[Int].reverse).take(3).sum

  def calculate(input: List[String]) = groupedElves(input)

  @tailrec
  def groupedElves(list: List[String], acc: List[Int] = Nil): List[Int] =
    if list.isEmpty then acc
    else {
      val (firstElf, rest) = list.span(_.nonEmpty)
      groupedElves(rest.drop(1), firstElf.map(_.toInt).sum :: acc)
    }

  def main(args: Array[String]): Unit =
    print("Part1: ")
    println(part1())
    print("Part2: ")
    println(part2())
