package com.jdriven.typedriven.aoc2022

import scala.annotation.tailrec
import scala.collection.immutable.{SortedSet, TreeMap}
import scala.collection.mutable
import scala.io.Source
import scala.language.postfixOps

object Day7:
  private lazy val part1Input = Source
    .fromResource("2022/day7-1.txt")
    .getLines()
    .toList

  def part1(input: List[String] = part1Input): Int = calculate(input)

  def part2(input: List[String] = part1Input): Int = calculate2(input)

  def calculate(input: List[String]): Int =
    val dir   = parse(input)._1
    val sizes = calculateSize(dir)
    sizes.filter(_ <= 100000).sum

  def calculate2(input: List[String]): Int =
    val dir         = parse(input)._1
    val fsSpace     = 70000000
    val currentSize = fsSpace - dir.size
    val minSpace    = 30000000
    calculateSize(dir)
      .filter(size => currentSize + size >= minSpace)
      .min

  private def calculateSize(dir: Dir, acc: List[Int] = Nil): List[Int] = dir.children.foldLeft(dir.size :: acc) {
    case (acc, f: File)  => acc
    case (acc, dir: Dir) => calculateSize(dir, acc)
  }

  private val dir  = "^dir\\h(.*)$".r
  private val file = "^(\\d+)\\h(.*)$".r
  private val ls   = "^\\$\\hls$".r
  private val cd   = "^\\$\\hcd\\h(.*)$".r

  def parse(input: List[String]): (Dir, List[String]) =
    input match {
      case cd(name) :: ls() :: rest =>
        val (files, lines) = lsDir(rest)
        val parsedDir      = Dir(name, files)
        parsedDir -> lines
    }

  @tailrec
  def lsDir(input: List[String], paths: SortedSet[Path] = SortedSet()): (SortedSet[Path], List[String]) = input match {
    case Nil                      => paths -> Nil
    case cd("..") :: rest         => paths -> rest
    case dir(name) :: rest        => lsDir(rest, paths)
    case file(size, name) :: rest => lsDir(rest, paths + File(name, size.toInt))
    case lines =>
      val (dir, rest) = parse(lines)
      lsDir(rest, paths + dir)
  }

  def main(args: Array[String]): Unit =
    print("Part1: ")
    println(part1())
    print("Part2: ")
    println(part2())

  sealed trait Path {
    def name: String
    def size: Int
  }

  object Path {
    given Ordering[Path] = (x: Path, y: Path) => x.name compareTo y.name
  }

  case class Dir(name: String, children: SortedSet[Path]) extends Path {
    def size: Int = children.map(_.size).sum
  }
  case class File(name: String, size: Int) extends Path
