package com.jdriven.typedriven.aoc2022

import scala.annotation.tailrec
import scala.collection.immutable.{SortedSet, TreeMap}
import scala.collection.mutable
import scala.io.Source
import scala.language.postfixOps

object Day8:
  private lazy val part1Input = Source
    .fromResource("2022/day8-1.txt")
    .getLines()
    .toList

  def part1(input: List[String] = part1Input): Int = calculate(input, State.forVisibility)(visibility).count
  def part2(input: List[String] = part1Input): Int = calculate(input, State.forView)(view).max

  case class State[S](trees: List[List[Int]], state: S, combinator: (Int, Int) => Int):
    def reversed   = copy(trees = trees.reverse)
    def transposed = copy(trees = trees.transpose)
    def count      = trees.flatten.count(_ >= 0)
    def max        = trees.flatten.max

    def combine(that: State[S]): State[S] = {
      val combinedTrees = (this.trees zip that.trees).map { case (a, b) => (a zip b).map(combinator.tupled) }
      State(combinedTrees, state, combinator)
    }

    override def toString: String = trees.map(_.mkString(" ")).mkString("", "\n", "\n")

  object State {
    def forVisibility(trees: List[List[Int]]) =
      val z0 = List.fill(trees.head.size)(-1)
      State(List(), z0, _ max _)

    def forView(trees: List[List[Int]]) =
      val z0 = List.iterate((0 to 9).map(_ -> 0).toMap, trees.head.size)(m => m)
      State(List(), z0, _ * _)
  }

  type fState[S] = (List[Int], State[S]) => State[S]
  def calculate[S](input: List[String], stateCreator: List[List[Int]] => State[S])(updateState: fState[S]) =
    val trees                  = input.map(_.map(_.toString.toInt).toList)
    val initialState: State[S] = stateCreator(trees)

    val topToBottom: State[S] = calcTopToBottom(updateState)(trees, initialState)
    val bottomToTop: State[S] = calcBottomToTop(updateState)(trees, initialState)

    val transposed            = trees.transpose
    val leftToRight: State[S] = calcTopToBottom(updateState)(transposed, initialState).transposed
    val rightToLeft: State[S] = calcBottomToTop(updateState)(transposed, initialState).transposed

    val combined = topToBottom combine bottomToTop combine leftToRight combine rightToLeft

//    println(topToBottom)
//    println(bottomToTop)
//    println(leftToRight)
//    println(rightToLeft)
//    println(combined)

    combined

  private def calcBottomToTop[S](updateState: fState[S])(trees: List[List[Int]], initialState: State[S]) =
    trees.foldRight(initialState) { case (row, state) => updateState(row, state) }

  private def calcTopToBottom[S](updateState: fState[S])(trees: List[List[Int]], initialState: State[S]) =
    trees.foldLeft(initialState) { case (state, row) => updateState(row, state) }.reversed

  private def visibility(row: List[Int], state: State[List[Int]]) = {
    val result  = row.zip(state.state).map { case (height, highest) => if height > highest then height else -1 }
    val highest = row.zip(state.state).map(_ max _)
    State(result :: state.trees, highest, state.combinator)
  }

  private def view(row: List[Int], state: State[List[Map[Int, Int]]]) = {
    val result = row.zip(state.state).map { case (height, views) => views(height) }
    val views  = row.zip(state.state).map{
      case (height, view) =>
        val (lower, higher) = view.partition(_._1 <= height)
        (lower.view.mapValues(_ => 1) ++ higher.view.mapValues(_ + 1)).toMap
    }
    State(result :: state.trees, views, state.combinator)
  }

  def main(args: Array[String]): Unit =
    print("Part1: ")
    println(part1())
    print("Part2: ")
    println(part2())
