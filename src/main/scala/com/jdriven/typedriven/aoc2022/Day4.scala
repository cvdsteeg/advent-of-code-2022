package com.jdriven.typedriven.aoc2022

import scala.io.Source
import scala.language.postfixOps

object Day4:
  val pattern = "(\\d+)-(\\d+),(\\d+)-(\\d+)" r
  private lazy val part1Input = Source
    .fromResource("2022/day4-1.txt")
    .getLines()
    .toList
    .filter(_.nonEmpty)

  def part1(input: List[String] = part1Input): Int = calculate(input)
  def part2(input: List[String] = part1Input): Int = calculate2(input)

  def calculate(input: List[String]) = input
    .map { case pattern(start1, end1, start2, end2) =>
      (start1.toInt to end1.toInt).toList -> (start2.toInt to end2.toInt).toList
    }
    .map { case (l1, l2) =>
      val intersect = l1 intersect l2
      if (intersect == l1 || intersect == l2) 1 else 0
    }
    .sum

  def calculate2(input: List[String]) = input
    .map { case pattern(start1, end1, start2, end2) =>
      (start1.toInt to end1.toInt).toList -> (start2.toInt to end2.toInt).toList
    }
    .map { case (l1, l2) =>
      val intersect = l1 intersect l2
      if (intersect.isEmpty) 0 else 1
    }
    .sum

  def main(args: Array[String]): Unit =
    print("Part1: ")
    println(part1())
    print("Part2: ")
    println(part2())
