package com.jdriven.typedriven.aoc2022

import scala.io.Source

object Day2:
  private lazy val part1Input = Source
    .fromResource("2022/day2-1.txt")
    .getLines()
    .toList
    .filter(_.nonEmpty)

  def part1(input: List[String] = part1Input): Int = calculate(input)(_._2)
  def part2(input: List[String] = part1Input): Int = calculate(input)(outcome)

  def calculate(input: List[String])(meaning: ((String, String)) => String) = input
    .to(LazyList)
    .map(_.split(" ").toList)
    .map { case opp :: me :: Nil => (opp, me) }
    .map { case (opp, me) => opp -> meaning(opp -> me) }
    .map {
      case (opp, "X") => opp -> "A"
      case (opp, "Y") => opp -> "B"
      case (opp, "Z") => opp -> "C"
      case tuple      => tuple
    }
    .map {
      case t @ (("C", "A") | ("A", "B") | ("B", "C")) => 6 -> t._2
      case (opp, me) if opp == me                     => 3 -> me
      case (_, me)                                    => 0 -> me
    }
    .map { case (score, me) => me.head - 'A' + 1 + score }
    .sum

  def outcome(game: (String, String)): String = game match {
    case ("A", "X") => "C"
    case (opp, "X") => (opp.head - 1).toChar.toString
    case (opp, "Y") => opp
    case ("C", "Z") => "A"
    case (opp, "Z") => (opp.head + 1).toChar.toString
  }

  def main(args: Array[String]): Unit =
    print("Part1: ")
    println(part1())
    print("Part2: ")
    println(part2())
