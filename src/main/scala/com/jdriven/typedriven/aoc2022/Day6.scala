package com.jdriven.typedriven.aoc2022

import scala.collection.immutable.TreeMap
import scala.io.Source

object Day6:
  private lazy val part1Input = Source
    .fromResource("2022/day6-1.txt")
    .getLines()
    .toList

  def part1(input: List[String] = part1Input): List[Int] = calculate(input, 4)

  def part2(input: List[String] = part1Input): List[Int] = calculate(input, 14)

  def calculate(input: List[String], sliding: Int): List[Int] =
    input.map {
      _.sliding(sliding)
        .takeWhile(s => s.distinct != s)
        .size + sliding
    }

  def main(args: Array[String]): Unit =
    print("Part1: ")
    println(part1())
    print("Part2: ")
    println(part2())
