package com.jdriven.typedriven.aoc2022

import scala.collection.immutable.TreeMap
import scala.io.Source
import scala.language.postfixOps

object Day5:
  type Containers = TreeMap[Int, String] // the order is important...
  private lazy val part1Input = Source
    .fromResource("2022/day5-1.txt")
    .getLines()
    .toList

  def part1(input: List[String] = part1Input): String = calculate(input, _.reverse)

  def part2(input: List[String] = part1Input): String = calculate(input, identity)

  def calculate(input: List[String], movingContainers: String => String): String =
    val pattern = "^move (\\d+) from (\\d+) to (\\d+)$" r

    val (stackLines, moveLines) = input.span(_.nonEmpty)

    val rows = stackLines.dropRight(1).map(_.grouped(4).toList.map(_.substring(1, 2)))
    val containers: Containers = rows.reverse.foldLeft(TreeMap.empty[Int, String]) { case (map, stack) =>
      stack.zipWithIndex.foldLeft(map) {
        case (m, (" ", _))           => m
        case (m, (container, index)) => m.updatedWith(index + 1)(stack => Some(container + stack.getOrElse("")))
      }
    }

    val moves = moveLines.drop(1).map { case pattern(n, from, to) => Move(n.toInt, from.toInt, to.toInt) }

    val result = moves.foldLeft(containers)(doMove(movingContainers))

    result.values.map(_.head).mkString

  private def doMove(movingContainers: String => String)(containers: Containers, move: Move): Containers = move match {
    case Move(amount, from, to) =>
//      println(containers)
//      println(move)

      val moving = movingContainers(containers(from) take amount)
      containers
        .updatedWith(from)(_.map(_.drop(amount)))
        .updatedWith(to)(_.map(stack => moving + stack))
  }

  def main(args: Array[String]): Unit =
    print("Part1: ")
    println(part1())
    print("Part2: ")
    println(part2())

  case class Move(amount: Int, from: Int, to: Int)
