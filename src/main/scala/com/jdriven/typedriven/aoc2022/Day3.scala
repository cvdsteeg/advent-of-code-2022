package com.jdriven.typedriven.aoc2022

import scala.io.Source

object Day3:
  private lazy val part1Input = Source
    .fromResource("2022/day3-1.txt")
    .getLines()
    .toList
    .filter(_.nonEmpty)

  def part1(input: List[String] = part1Input): Int = calculate(input)
  def part2(input: List[String] = part1Input): Int = calculate2(input)

  def calculate(input: List[String]) = input
    .map(_.map {
      case ch if ch.isLower => ch - 'a' + 1
      case ch               => ch - 'A' + 27
    })
    .map(s => s.splitAt(s.size / 2))
    .map { case (first, second) => first intersect second }
    .map(_.distinct.sum)
    .sum

  def calculate2(input: List[String]) = input
    .map(_.map {
      case ch if ch.isLower => ch - 'a' + 1
      case ch               => ch - 'A' + 27
    }.toList)
    .grouped(3)
    .map(_.reduce(_ intersect _))
    .map(_.distinct.sum)
    .sum

  def main(args: Array[String]): Unit =
    print("Part1: ")
    println(part1())
    println("Part2: ")
    println(part2())
