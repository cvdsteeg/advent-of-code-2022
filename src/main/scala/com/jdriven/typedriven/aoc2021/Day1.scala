package com.jdriven.typedriven.aoc2021

import scala.io.Source

object Day1:
  private lazy val part1Input = Source
    .fromResource("2021/day1-1.txt")
    .getLines()
    .toList
    .filter(_.nonEmpty)

  def part1(input: List[String] = part1Input): Int = calculation(1)(input)
  def part2(input: List[String] = part1Input): Int = calculation(3)(input)

  def calculation(window: Int)(input: List[String] = part1Input): Int =
    input
      .map(_.toInt)
      .sliding(window)
      .map(_.sum)
      .sliding(2)
      .map(_.toList)
      .map {
        case e1 :: e2 :: Nil => e2 - e1
      }
      .count(_ > 0)

  def main(args: Array[String]): Unit =
    println(s"Part1: ${part1()}")
    println(s"Part2: ${part2()}")
