package com.jdriven.typedriven

import zio.*
import zio.Console.printLine

object Main extends ZIOAppDefault:
  override def run = printLine("Welcome to your first ZIO app!")
